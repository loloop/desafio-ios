//
//  PullRequestListTableViewDelegate.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/3/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestListTableViewDelegate: NSObject {
    let controller: PullRequestViewController

    init(_ controller: PullRequestViewController) {
        self.controller = controller
    }
}

extension PullRequestListTableViewDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PullRequestTableViewCell.rowHeight
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return PullRequestTableViewCell.rowHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: controller.requests[indexPath.row].urlString) else {
            return
        }

        if #available(iOS 9.0, *) {
            let safariView = SFSafariViewController(url: url)
            controller.present(safariView, animated: true, completion: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentSize.height - scrollView.contentOffset.y < scrollView.frame.size.height {
            self.controller.fetchRequests()
        }
    }

}
