//
//  RepositoryListTableViewDelegate.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/4/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation
import UIKit

class RepositoryListTableViewDelegate: NSObject {

    weak var controller: RepositoryListViewController?

    init(_ controller: RepositoryListViewController) {
        self.controller = controller
    }
}

extension RepositoryListTableViewDelegate: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let repo = controller?.repositories[indexPath.row] else {
            return
        }

        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        if let prView = storyboard.instantiateViewController(withIdentifier: "PullRequestViewController")
            as? PullRequestViewController {
            prView.repository = repo
            self.controller?.navigationController?.pushViewController(prView, animated: true)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositoryListTableViewCell.rowHeight
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositoryListTableViewCell.rowHeight
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentSize.height - scrollView.contentOffset.y < scrollView.frame.size.height {
            controller?.fetchRepositories()
        }
    }

}
