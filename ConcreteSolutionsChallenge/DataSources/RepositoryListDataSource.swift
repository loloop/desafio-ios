//
//  RepositoryListDataSource.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import UIKit

class RepositoryListDataSource: NSObject {
    var data: [Repository]
    fileprivate let cellIdentifier =  "RepositoryListTableViewCell"

    init(_ tableView: UITableView, repositories: [Repository]) {
        tableView.register(UINib.init(nibName: cellIdentifier, bundle: Bundle.main),
                           forCellReuseIdentifier: cellIdentifier)
        data = repositories
    }
}

extension RepositoryListDataSource: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repository = data[indexPath.row]

        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                                                                    as? RepositoryListTableViewCell {
            cell.setRepository(for: repository)
            return cell
        } else {
            let repoCell = RepositoryListTableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            repoCell.setRepository(for: repository)
            return repoCell
        }
    }
}
