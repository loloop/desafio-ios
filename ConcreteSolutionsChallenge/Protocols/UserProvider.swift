//
//  UserProvider.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/3/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

protocol UserProvider {
    var session: URLSession { get set }
    func fetchUser(for repository: Repository, result: @escaping (Result<GitHubUser>) -> Void)
    func fetchUser(for request: PullRequest, result: @escaping (Result<GitHubUser>) -> Void)
    func fetchUser(username: String, result: @escaping (Result<GitHubUser>) -> Void)
}
