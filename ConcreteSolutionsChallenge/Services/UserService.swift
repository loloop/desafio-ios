//
//  UserService.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/5/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

class UserService {
    fileprivate var mSession: URLSession?
}

extension UserService: UserProvider {
    var session: URLSession {
        get {
            return mSession ?? URLSession.shared
        }
        set {
            self.mSession = newValue
        }
    }

    func fetchUser(for request: PullRequest, result: @escaping (Result<GitHubUser>) -> Void) {
        fetchUser(username: request.author.username, result: result)
    }

    func fetchUser(for repository: Repository, result: @escaping (Result<GitHubUser>) -> Void) {
        fetchUser(username: repository.author.username, result: result)
    }

    func fetchUser(username: String, result: @escaping (Result<GitHubUser>) -> Void) {
        session.dataTask(with: GitHub.composeUserURL(username: username)) { (data, _, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    print("data not data")
                    result(.error(UserServiceError()))
                }
                return
            }

            do {
                let user = try GitHubUser(from: data)
                DispatchQueue.main.async {
                    result(.success(user))
                }
            } catch {
                DispatchQueue.main.async {
                    print("n conseguiu gerar ghuser")
                    result(.error(UserServiceError()))
                }
            }
        }.resume()
    }
}

struct UserServiceError: Error {}
