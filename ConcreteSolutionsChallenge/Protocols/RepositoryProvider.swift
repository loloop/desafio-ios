//
//  RepositoryProvider.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

protocol RepositoryProvider {
    var session: URLSession { get set }
    func fetch(language: ProgrammingLanguages, pageNumber: Int, result: @escaping (Result<[Repository]>) -> Void)
}
