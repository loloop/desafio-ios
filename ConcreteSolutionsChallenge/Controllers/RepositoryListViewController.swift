//
//  RepositoryListViewController.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/2/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation
import UIKit

class RepositoryListViewController: UIViewController {

    var repositories: [Repository] = []
    var dataSource: RepositoryListDataSource?
    var provider: RepositoryProvider = GitHubRepositoryService()
    // swiftlint:disable weak_delegate
    var tableViewDelegate: RepositoryListTableViewDelegate?

    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stateLabel: UILabel!

    var currentState: ViewState = .loading {
        didSet {
            switch currentState {
            case .loading:
                self.stateView.isHidden = false
                self.tableView.isHidden  = true
                self.stateLabel.text = "Carregando"
            case .ready:
                self.stateView.isHidden = true
                self.tableView.isHidden = false
                self.tableView.reloadData()
            case .error(let message):
                self.stateView.isHidden = false
                self.tableView.isHidden = true
                self.stateLabel.text = message
            case .empty:
                self.stateView.isHidden = false
                self.tableView.isHidden = true
                self.stateLabel.text = "Ocorreu um erro ao carregar os repositórios"
            case .paginating:
                // just for state representation
                break
            }
        }
    }

    override func viewDidLoad() {
        fetchRepositories()
    }

    fileprivate var currentPage: Int = 0
    func fetchRepositories() {
        if currentState == .paginating {
            return
        }
        currentState = .paginating
        currentPage += 1
        provider.fetch(language: .java, pageNumber: currentPage) { (result) in
            switch result {
            case .success(let repositories):
                self.repositories += repositories
                if self.dataSource != nil, self.tableViewDelegate != nil {
                    self.dataSource?.data += repositories
                } else {
                    self.dataSource = RepositoryListDataSource(self.tableView, repositories: repositories)
                    self.tableViewDelegate = RepositoryListTableViewDelegate(self)
                    self.tableView.dataSource = self.dataSource!
                    self.tableView.delegate = self.tableViewDelegate
                }

                self.currentState = .ready
            case .error(let error):
                guard let error = error as? RepositoryError else {
                    self.currentState = .error("Ocorreu um erro desconhecido")
                    return
                }
                self.currentState = .error(error.description)
            }
        }
    }

    fileprivate func setupView() {
        self.stateView.isHidden = true
        if #available(iOS 11, *) {
            self.navigationItem.largeTitleDisplayMode = .automatic
        }
    }
}
