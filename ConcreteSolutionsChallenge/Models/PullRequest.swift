//
//  PullRequest.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

struct PullRequest: Decodable, DataDecodable {

    enum Status: String {
        case open
        case closed
        case error
    }

    var author: GitHubUser
    let title: String
    let description: String
    let currentStatus: Status
    let urlString: String
    let dateCreated: String

    init(from decoder: Decoder) throws {
        let rawRequest = try RawPullRequest(from: decoder)

        self.title = rawRequest.title
        self.description = rawRequest.body ?? ""
        self.currentStatus = PullRequest.Status(rawValue: rawRequest.state) ?? .error
        self.urlString = rawRequest.html_url
        self.author = rawRequest.user

        // date conversion
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .short
        formatter.isLenient = true
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        if let date = formatter.date(from: rawRequest.created_at) {
            formatter.dateFormat = "dd/MM/yyyy"
            self.dateCreated = formatter.string(from: date)
        } else {
            self.dateCreated = ""
        }
    }
}

extension PullRequest {
    // swiftlint:disable identifier_name
    fileprivate struct RawPullRequest: Codable {
        let title: String
        let state: String
        let body: String?
        let html_url: String
        let user: GitHubUser
        let created_at: String
    }
}

// swiftlint:disable line_length
/*
 {
 "url": "https://api.github.com/repos/square/okhttp/pulls/3668",
 "id": 149151915,
 "html_url": "https://github.com/square/okhttp/pull/3668",
 "diff_url": "https://github.com/square/okhttp/pull/3668.diff",
 "patch_url": "https://github.com/square/okhttp/pull/3668.patch",
 "issue_url": "https://api.github.com/repos/square/okhttp/issues/3668",
 "number": 3668,
 "state": "open",
 "locked": false,
 "title": "We should always have our callbacks called",
 "user": {
     "login": "j-baker",
     "id": 42533,
     "avatar_url": "https://avatars2.githubusercontent.com/u/42533?v=4",
     "gravatar_id": "",
     "url": "https://api.github.com/users/j-baker",
     "html_url": "https://github.com/j-baker",
     "followers_url": "https://api.github.com/users/j-baker/followers",
     "following_url": "https://api.github.com/users/j-baker/following{/other_user}",
     "gists_url": "https://api.github.com/users/j-baker/gists{/gist_id}",
     "starred_url": "https://api.github.com/users/j-baker/starred{/owner}{/repo}",
     "subscriptions_url": "https://api.github.com/users/j-baker/subscriptions",
     "organizations_url": "https://api.github.com/users/j-baker/orgs",
     "repos_url": "https://api.github.com/users/j-baker/repos",
     "events_url": "https://api.github.com/users/j-baker/events{/privacy}",
     "received_events_url": "https://api.github.com/users/j-baker/received_events",
     "type": "User",
     "site_admin": false
 },
 "body": "A common user error with OkHttp is to write an interceptor which might\r\nthrow an IllegalArgumentException or similar. This works fine when using\r\nthe 'execute()' interface, but with 'enqueue()' leads to serious\r\nproblems; callbacks might never be fired.\r\n\r\nThis leads to very subtle bugs. For example, the Retrofit Java 8 call\r\nadapter returns a CompletableFuture, which might never be completed.\r\n\r\nHere, we wrap every throwable with an IOException to ensure that the\r\ncallback fires.\r\n\r\nAppreciate this might need some changes - throwing it up to get feedback.",
 "created_at": "2017-10-27T10:18:47Z",
 "updated_at": "2017-10-30T18:10:34Z",
 "closed_at": null,
 "merged_at": null,
 "merge_commit_sha": "5cd161194829382d9a0c210aa2bf3d36ad70e396",
 "assignee": null,
 "assignees": [

 ],
 "requested_reviewers": [

 ],
 "milestone": null,
 "commits_url": "https://api.github.com/repos/square/okhttp/pulls/3668/commits",
 "review_comments_url": "https://api.github.com/repos/square/okhttp/pulls/3668/comments",
 "review_comment_url": "https://api.github.com/repos/square/okhttp/pulls/comments{/number}",
 "comments_url": "https://api.github.com/repos/square/okhttp/issues/3668/comments",
 "statuses_url": "https://api.github.com/repos/square/okhttp/statuses/6a3db724b8aa523bea9fac703bdc493809255e6e",
 "head": {
 "label": "j-baker:jbaker/callback_should_throw",
 "ref": "jbaker/callback_should_throw",
 "sha": "6a3db724b8aa523bea9fac703bdc493809255e6e",
 "user": {
 "login": "j-baker",
 "id": 42533,
 "avatar_url": "https://avatars2.githubusercontent.com/u/42533?v=4",
 "gravatar_id": "",
 "url": "https://api.github.com/users/j-baker",
 "html_url": "https://github.com/j-baker",
 "followers_url": "https://api.github.com/users/j-baker/followers",
 "following_url": "https://api.github.com/users/j-baker/following{/other_user}",
 "gists_url": "https://api.github.com/users/j-baker/gists{/gist_id}",
 "starred_url": "https://api.github.com/users/j-baker/starred{/owner}{/repo}",
 "subscriptions_url": "https://api.github.com/users/j-baker/subscriptions",
 "organizations_url": "https://api.github.com/users/j-baker/orgs",
 "repos_url": "https://api.github.com/users/j-baker/repos",
 "events_url": "https://api.github.com/users/j-baker/events{/privacy}",
 "received_events_url": "https://api.github.com/users/j-baker/received_events",
 "type": "User",
 "site_admin": false
 },
 "repo": {
 "id": 108523558,
 "name": "okhttp",
 "full_name": "j-baker/okhttp",
 "owner": {
 "login": "j-baker",
 "id": 42533,
 "avatar_url": "https://avatars2.githubusercontent.com/u/42533?v=4",
 "gravatar_id": "",
 "url": "https://api.github.com/users/j-baker",
 "html_url": "https://github.com/j-baker",
 "followers_url": "https://api.github.com/users/j-baker/followers",
 "following_url": "https://api.github.com/users/j-baker/following{/other_user}",
 "gists_url": "https://api.github.com/users/j-baker/gists{/gist_id}",
 "starred_url": "https://api.github.com/users/j-baker/starred{/owner}{/repo}",
 "subscriptions_url": "https://api.github.com/users/j-baker/subscriptions",
 "organizations_url": "https://api.github.com/users/j-baker/orgs",
 "repos_url": "https://api.github.com/users/j-baker/repos",
 "events_url": "https://api.github.com/users/j-baker/events{/privacy}",
 "received_events_url": "https://api.github.com/users/j-baker/received_events",
 "type": "User",
 "site_admin": false
 },
 "private": false,
 "html_url": "https://github.com/j-baker/okhttp",
 "description": "An HTTP+HTTP/2 client for Android and Java applications.",
 "fork": true,
 "url": "https://api.github.com/repos/j-baker/okhttp",
 "forks_url": "https://api.github.com/repos/j-baker/okhttp/forks",
 "keys_url": "https://api.github.com/repos/j-baker/okhttp/keys{/key_id}",
 "collaborators_url": "https://api.github.com/repos/j-baker/okhttp/collaborators{/collaborator}",
 "teams_url": "https://api.github.com/repos/j-baker/okhttp/teams",
 "hooks_url": "https://api.github.com/repos/j-baker/okhttp/hooks",
 "issue_events_url": "https://api.github.com/repos/j-baker/okhttp/issues/events{/number}",
 "events_url": "https://api.github.com/repos/j-baker/okhttp/events",
 "assignees_url": "https://api.github.com/repos/j-baker/okhttp/assignees{/user}",
 "branches_url": "https://api.github.com/repos/j-baker/okhttp/branches{/branch}",
 "tags_url": "https://api.github.com/repos/j-baker/okhttp/tags",
 "blobs_url": "https://api.github.com/repos/j-baker/okhttp/git/blobs{/sha}",
 "git_tags_url": "https://api.github.com/repos/j-baker/okhttp/git/tags{/sha}",
 "git_refs_url": "https://api.github.com/repos/j-baker/okhttp/git/refs{/sha}",
 "trees_url": "https://api.github.com/repos/j-baker/okhttp/git/trees{/sha}",
 "statuses_url": "https://api.github.com/repos/j-baker/okhttp/statuses/{sha}",
 "languages_url": "https://api.github.com/repos/j-baker/okhttp/languages",
 "stargazers_url": "https://api.github.com/repos/j-baker/okhttp/stargazers",
 "contributors_url": "https://api.github.com/repos/j-baker/okhttp/contributors",
 "subscribers_url": "https://api.github.com/repos/j-baker/okhttp/subscribers",
 "subscription_url": "https://api.github.com/repos/j-baker/okhttp/subscription",
 "commits_url": "https://api.github.com/repos/j-baker/okhttp/commits{/sha}",
 "git_commits_url": "https://api.github.com/repos/j-baker/okhttp/git/commits{/sha}",
 "comments_url": "https://api.github.com/repos/j-baker/okhttp/comments{/number}",
 "issue_comment_url": "https://api.github.com/repos/j-baker/okhttp/issues/comments{/number}",
 "contents_url": "https://api.github.com/repos/j-baker/okhttp/contents/{+path}",
 "compare_url": "https://api.github.com/repos/j-baker/okhttp/compare/{base}...{head}",
 "merges_url": "https://api.github.com/repos/j-baker/okhttp/merges",
 "archive_url": "https://api.github.com/repos/j-baker/okhttp/{archive_format}{/ref}",
 "downloads_url": "https://api.github.com/repos/j-baker/okhttp/downloads",
 "issues_url": "https://api.github.com/repos/j-baker/okhttp/issues{/number}",
 "pulls_url": "https://api.github.com/repos/j-baker/okhttp/pulls{/number}",
 "milestones_url": "https://api.github.com/repos/j-baker/okhttp/milestones{/number}",
 "notifications_url": "https://api.github.com/repos/j-baker/okhttp/notifications{?since,all,participating}",
 "labels_url": "https://api.github.com/repos/j-baker/okhttp/labels{/name}",
 "releases_url": "https://api.github.com/repos/j-baker/okhttp/releases{/id}",
 "deployments_url": "https://api.github.com/repos/j-baker/okhttp/deployments",
 "created_at": "2017-10-27T08:59:58Z",
 "updated_at": "2017-10-27T09:00:05Z",
 "pushed_at": "2017-10-27T10:24:34Z",
 "git_url": "git://github.com/j-baker/okhttp.git",
 "ssh_url": "git@github.com:j-baker/okhttp.git",
 "clone_url": "https://github.com/j-baker/okhttp.git",
 "svn_url": "https://github.com/j-baker/okhttp",
 "homepage": "http://square.github.io/okhttp/",
 "size": 16413,
 "stargazers_count": 0,
 "watchers_count": 0,
 "language": "Java",
 "has_issues": false,
 "has_projects": true,
 "has_downloads": true,
 "has_wiki": true,
 "has_pages": false,
 "forks_count": 0,
 "mirror_url": null,
 "archived": false,
 "open_issues_count": 0,
 "forks": 0,
 "open_issues": 0,
 "watchers": 0,
 "default_branch": "master"
 }
 },
 "base": {
 "label": "square:master",
 "ref": "master",
 "sha": "d9091b357bb74ba72b57953d2dfc7bcbbd099533",
 "user": {
 "login": "square",
 "id": 82592,
 "avatar_url": "https://avatars0.githubusercontent.com/u/82592?v=4",
 "gravatar_id": "",
 "url": "https://api.github.com/users/square",
 "html_url": "https://github.com/square",
 "followers_url": "https://api.github.com/users/square/followers",
 "following_url": "https://api.github.com/users/square/following{/other_user}",
 "gists_url": "https://api.github.com/users/square/gists{/gist_id}",
 "starred_url": "https://api.github.com/users/square/starred{/owner}{/repo}",
 "subscriptions_url": "https://api.github.com/users/square/subscriptions",
 "organizations_url": "https://api.github.com/users/square/orgs",
 "repos_url": "https://api.github.com/users/square/repos",
 "events_url": "https://api.github.com/users/square/events{/privacy}",
 "received_events_url": "https://api.github.com/users/square/received_events",
 "type": "Organization",
 "site_admin": false
 },
 "repo": {
 "id": 5152285,
 "name": "okhttp",
 "full_name": "square/okhttp",
 "owner": {
 "login": "square",
 "id": 82592,
 "avatar_url": "https://avatars0.githubusercontent.com/u/82592?v=4",
 "gravatar_id": "",
 "url": "https://api.github.com/users/square",
 "html_url": "https://github.com/square",
 "followers_url": "https://api.github.com/users/square/followers",
 "following_url": "https://api.github.com/users/square/following{/other_user}",
 "gists_url": "https://api.github.com/users/square/gists{/gist_id}",
 "starred_url": "https://api.github.com/users/square/starred{/owner}{/repo}",
 "subscriptions_url": "https://api.github.com/users/square/subscriptions",
 "organizations_url": "https://api.github.com/users/square/orgs",
 "repos_url": "https://api.github.com/users/square/repos",
 "events_url": "https://api.github.com/users/square/events{/privacy}",
 "received_events_url": "https://api.github.com/users/square/received_events",
 "type": "Organization",
 "site_admin": false
 },
 "private": false,
 "html_url": "https://github.com/square/okhttp",
 "description": "An HTTP+HTTP/2 client for Android and Java applications.",
 "fork": false,
 "url": "https://api.github.com/repos/square/okhttp",
 "forks_url": "https://api.github.com/repos/square/okhttp/forks",
 "keys_url": "https://api.github.com/repos/square/okhttp/keys{/key_id}",
 "collaborators_url": "https://api.github.com/repos/square/okhttp/collaborators{/collaborator}",
 "teams_url": "https://api.github.com/repos/square/okhttp/teams",
 "hooks_url": "https://api.github.com/repos/square/okhttp/hooks",
 "issue_events_url": "https://api.github.com/repos/square/okhttp/issues/events{/number}",
 "events_url": "https://api.github.com/repos/square/okhttp/events",
 "assignees_url": "https://api.github.com/repos/square/okhttp/assignees{/user}",
 "branches_url": "https://api.github.com/repos/square/okhttp/branches{/branch}",
 "tags_url": "https://api.github.com/repos/square/okhttp/tags",
 "blobs_url": "https://api.github.com/repos/square/okhttp/git/blobs{/sha}",
 "git_tags_url": "https://api.github.com/repos/square/okhttp/git/tags{/sha}",
 "git_refs_url": "https://api.github.com/repos/square/okhttp/git/refs{/sha}",
 "trees_url": "https://api.github.com/repos/square/okhttp/git/trees{/sha}",
 "statuses_url": "https://api.github.com/repos/square/okhttp/statuses/{sha}",
 "languages_url": "https://api.github.com/repos/square/okhttp/languages",
 "stargazers_url": "https://api.github.com/repos/square/okhttp/stargazers",
 "contributors_url": "https://api.github.com/repos/square/okhttp/contributors",
 "subscribers_url": "https://api.github.com/repos/square/okhttp/subscribers",
 "subscription_url": "https://api.github.com/repos/square/okhttp/subscription",
 "commits_url": "https://api.github.com/repos/square/okhttp/commits{/sha}",
 "git_commits_url": "https://api.github.com/repos/square/okhttp/git/commits{/sha}",
 "comments_url": "https://api.github.com/repos/square/okhttp/comments{/number}",
 "issue_comment_url": "https://api.github.com/repos/square/okhttp/issues/comments{/number}",
 "contents_url": "https://api.github.com/repos/square/okhttp/contents/{+path}",
 "compare_url": "https://api.github.com/repos/square/okhttp/compare/{base}...{head}",
 "merges_url": "https://api.github.com/repos/square/okhttp/merges",
 "archive_url": "https://api.github.com/repos/square/okhttp/{archive_format}{/ref}",
 "downloads_url": "https://api.github.com/repos/square/okhttp/downloads",
 "issues_url": "https://api.github.com/repos/square/okhttp/issues{/number}",
 "pulls_url": "https://api.github.com/repos/square/okhttp/pulls{/number}",
 "milestones_url": "https://api.github.com/repos/square/okhttp/milestones{/number}",
 "notifications_url": "https://api.github.com/repos/square/okhttp/notifications{?since,all,participating}",
 "labels_url": "https://api.github.com/repos/square/okhttp/labels{/name}",
 "releases_url": "https://api.github.com/repos/square/okhttp/releases{/id}",
 "deployments_url": "https://api.github.com/repos/square/okhttp/deployments",
 "created_at": "2012-07-23T13:42:55Z",
 "updated_at": "2017-11-03T02:50:19Z",
 "pushed_at": "2017-10-27T10:24:38Z",
 "git_url": "git://github.com/square/okhttp.git",
 "ssh_url": "git@github.com:square/okhttp.git",
 "clone_url": "https://github.com/square/okhttp.git",
 "svn_url": "https://github.com/square/okhttp",
 "homepage": "http://square.github.io/okhttp/",
 "size": 16410,
 "stargazers_count": 23232,
 "watchers_count": 23232,
 "language": "Java",
 "has_issues": true,
 "has_projects": false,
 "has_downloads": true,
 "has_wiki": true,
 "has_pages": true,
 "forks_count": 5560,
 "mirror_url": null,
 "archived": false,
 "open_issues_count": 129,
 "forks": 5560,
 "open_issues": 129,
 "watchers": 23232,
 "default_branch": "master"
 }
 },
 "_links": {
 "self": {
 "href": "https://api.github.com/repos/square/okhttp/pulls/3668"
 },
 "html": {
 "href": "https://github.com/square/okhttp/pull/3668"
 },
 "issue": {
 "href": "https://api.github.com/repos/square/okhttp/issues/3668"
 },
 "comments": {
 "href": "https://api.github.com/repos/square/okhttp/issues/3668/comments"
 },
 "review_comments": {
 "href": "https://api.github.com/repos/square/okhttp/pulls/3668/comments"
 },
 "review_comment": {
 "href": "https://api.github.com/repos/square/okhttp/pulls/comments{/number}"
 },
 "commits": {
 "href": "https://api.github.com/repos/square/okhttp/pulls/3668/commits"
 },
 "statuses": {
 "href": "https://api.github.com/repos/square/okhttp/statuses/6a3db724b8aa523bea9fac703bdc493809255e6e"
 }
 },
 "author_association": "NONE"
 },


 */
