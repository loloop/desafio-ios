//
//  GitHubService.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

class GitHubRepositoryService {
    fileprivate var mSession: URLSession?
}

extension GitHubRepositoryService: RepositoryProvider {
    var session: URLSession {
        get {
            return mSession ?? URLSession.shared
        }
        set {
            self.mSession = newValue
        }
    }

    func fetch(language: ProgrammingLanguages, pageNumber: Int = 1, result: @escaping (Result<[Repository]>) -> Void) {
        session.dataTask(
        with: GitHub.composeURL(language: language,
                                sort: .stars,
                                pageNumber: pageNumber)) { data, _, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    result(.error(RepositoryError()))
                }
                return
            }

            do {
                let repos = try Repositories(from: data)
                DispatchQueue.main.async {
                    return result(.success(repos.items))
                }
            } catch DecodingError.dataCorrupted(_) {
                DispatchQueue.main.async {
                    return result(.error(RepositoryError()))
                }
            } catch {
                dump(error)
            }
        }.resume()
    }
}

struct RepositoryError: Error {
    let description: String = "Ocorreu um erro de conexão"
}
