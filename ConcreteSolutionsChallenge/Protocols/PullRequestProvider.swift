//
//  PullRequestProvider.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

protocol PullRequestProvider {
    var session: URLSession { get set }
    func fetchRequests(for repository: Repository, pageNumber: Int, result: @escaping (Result<[PullRequest]>) -> Void)
    func fetchRequestStatus(for repository: Repository, status: GitHub.PRStatus, result:
        @escaping (Result<RequestStatus>) -> Void)
}
