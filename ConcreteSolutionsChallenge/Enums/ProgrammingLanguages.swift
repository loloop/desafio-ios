//
//  ProgrammingLanguages.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

enum ProgrammingLanguages: String {
    case java = "Java"
    case unknown = "Unknown"
}
