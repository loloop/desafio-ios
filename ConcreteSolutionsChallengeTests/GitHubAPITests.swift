//
//  GitHubAPITests.swift
//  ConcreteSolutionsChallengeTests
//
//  Created by Mauricio Cardozo on 11/1/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import XCTest
@testable import ConcreteSolutionsChallenge

class GitHubAPITests: XCTestCase {

    let rxjavaUrlString = "https://api.github.com/repos/ReactiveX/RxJava/pulls?state=all&page=1"
    let okhttpUrlString = "https://api.github.com/repos/square/okhttp/pulls?state=all&page=1"

    var repository: Repository!

    override func setUp() {
        let mockData = MockData()
        guard let mockRepo = mockData.getRepositoryFromJSON() else {
            XCTFail("MockData repository failed")
            return
        }
        do {
            let repo = try Repository(from: mockRepo)
            self.repository = repo
        } catch(let error) {
            XCTFail(error.localizedDescription)
        }
    }

    func testShouldComposeURLWithDefaultValues() {
        let url = GitHub.composeURL(language: .java)
        XCTAssertEqual("https://api.github.com/search/repositories?q=language:java&sort=forks&order=desc&page=1",
                       url.absoluteString)
    }

    func testShouldComposeLanguageURLWithStarSorting() {
        let url = GitHub.composeURL(language: .java, sort: .stars)
        XCTAssertEqual("https://api.github.com/search/repositories?q=language:java&sort=stars&order=desc&page=1",
                       url.absoluteString)
    }

    func testShouldComposeLanguageURLWithStarSortingAndAscendingOrder() {
        let url = GitHub.composeURL(language: .java, sort: .stars, order: .ascending)
        XCTAssertEqual("https://api.github.com/search/repositories?q=language:java&sort=stars&order=asc&page=1",
                       url.absoluteString)
    }

    func testShouldComposeLanguageURLWithStarSortingAscendingOrderAndPageNumber() {
        let url = GitHub.composeURL(language: .java, sort: .stars, order: .ascending, pageNumber: 2)
        XCTAssertEqual("https://api.github.com/search/repositories?q=language:java&sort=stars&order=asc&page=2",
                       url.absoluteString)
    }

    func testShouldComposePullRequestURLWithOwnerAndRepo() {
        let url = GitHub.composeURL(owner: "ReactiveX", repo: "RxJava")
        XCTAssertEqual(rxjavaUrlString, url.absoluteString)
    }

    func testShouldComposePullRequestURLWithPageNumber() {
        let url = GitHub.composeURL(owner: "ReactiveX", repo: "RxJava", pageNumber: 2)
        let rxJavaPagedString = "https://api.github.com/repos/ReactiveX/RxJava/pulls?state=all&page=2"
        XCTAssertEqual(rxJavaPagedString, url.absoluteString)
    }

    func testShouldComposePullRequestCountURLForClosedRequests() {
        let url = GitHub.composeURL(for: repository, status: .closed)
        XCTAssertEqual("https://api.github.com/search/issues?q=type:pr+repo:reactivex/rxjava+is:closed",
                       url.absoluteString)
    }

    func testShouldComposePullRequestCountURLForOpenRequests() {
        let url = GitHub.composeURL(for: repository, status: .open)
        XCTAssertEqual("https://api.github.com/search/issues?q=type:pr+repo:reactivex/rxjava+is:open",
                       url.absoluteString)
    }

    func testShouldComposePullRequestURLWithRepositoryObject() {
        let url = GitHub.composeURL(for: repository)
        XCTAssertEqual(rxjavaUrlString, url.absoluteString)
    }

    func testShouldComposeUserURLWithRepositoryObject() {
        let url = GitHub.composeUserURL(for: repository)
        XCTAssertEqual("https://api.github.com/users/reactivex", url.absoluteString)
    }

    func testShouldComposeUserURLWithUsernameString() {
        let url = GitHub.composeUserURL(username: "loloop")
        XCTAssertEqual("https://api.github.com/users/loloop", url.absoluteString)
    }

}
