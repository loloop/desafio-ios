// 
//  RepositoryListTableViewCell.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/4/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import UIKit
import Foundation

class RepositoryListTableViewCell: UITableViewCell {

    static let rowHeight = CGFloat(120)

    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var body: UILabel!
    @IBOutlet private weak var forks: UILabel!
    @IBOutlet private weak var stars: UILabel!
    @IBOutlet private weak var authorPhoto: UIImageView!
    @IBOutlet private weak var authorName: UILabel!

    func setRepository(for repository: Repository) {
        self.title.text = repository.name
        self.body.text = repository.description
        self.forks.text = String(repository.forks)
        self.stars.text = String(repository.stars)
        self.authorName.text = repository.author.username
        repository.author.avatarImage { (image) in
            guard let image = image else {
                return // xib has a default image for this value
            }
            self.authorPhoto.image = image
        }
    }

}
